# Mezos — Middleware Tezos for Mobile Devices and more

**Warning:** *Refactoring in progress*

## Description

Mezos provides a middleware for mobile devices.
Technically, it connects to a PostGres database filled by
[`tezos-indexer`](https://gitlab.com/nomadic-labs/tezos-indexer) and
provides a set of RPC for mobile devices to use.
It also connects directly to the tezos-node for some wallet operations;
and that's the reason why you can't run the same Mezos code for all Tezos networks.


## Installation

Once you have `tezos-node` and `tezos-indexer` running, it's easy to run Mezos
(from the machine that hosts `tezos-indexer`):

Get the source code:
```
git clone https://gitlab.com/nomadic-labs/mezos -b BabyM1-CARTHA
cd mezos
make source-deps
```

Then build for the babylon (BabyM1) protocol:
```
make 5
```

or for the carthage protocol:
```
make 6
```

If you need to run mezos with another protocol, [please open an issue](https://gitlab.com/nomadic-labs/mezos/issues)!

And run it:
```
./mezos.exe run
```

N.B. About the protocols: at time of writing, Babylonnet uses only protocol 005_BabyM1, which is supported.
Then, Mainnet currently uses protocol 005_BabyM1, and Carthagenet protocol 006_CARTHA.
The thing you have to understand: Mezos requires to be compiled for the current protocol of a given network, not future ones, not past ones.
Protocol transition blocks are not supported. If Mezos is compiled against a not current protocol, it might still partially work.

### Running on a machine that doesn't run tezos-indexer

If you want to run Mezos without running tezos-indexer, you should
still follow the [building process of tezos-indexer](https://gitlab.com/nomadic-labs/tezos-indexer/blob/multinetwork-dev/README.md) because its
dependencies are required for Mezos.



## REST wallet

The REST wallet does two things:

* Provide REST access to `tezos-client`.
* Add a local BIP32 wallet.

The first feature is implemented with the `tezos-client` libs, using
`ocplib-resto` for implementing the REST server and services as it is
done in Tezos.

The second feature leverages `ocaml-bip32-ed25519`, itself built on
`ocaml-monocypher`, a neat cryptographic library. This has been done
as a temporary measure since BIP32 was not a standard feature of the
Tezos client yet.

With Mezos, it is possible to generate a mnemonic, encrypt it in a
SQLite DB, and then derive arbitrary addresses using the BI32-Ed25519
scheme. This feature is useful if you want for example host a web
service that need to create a new Tezos address for each customer.

### How signing is performed

This software uses the remote signing scheme of Tezos client in order
to be able to perform its own signature. It implements a local socket
signer and instruct the Tezos client functions to use it as its
signer. Upon receiving signing requests from Tezos-client, it will
sign them using either:

* its local BIP32 wallet
* remote clients, that will perform signature themselves

In the latter case, there is a two-step REST API directed towards the
remote client. The first API call (`/transfer/init`) is used by the
remote client to request a transfer, whereas the second call
(`/transfer/finalize`) is used to finalize the transfer: the remote
client will provide the signature.
