
-- DROP FUNCTION contracts2(character varying);
CREATE OR REPLACE FUNCTION contracts2 (x varchar)
RETURNS TABLE(k char, bal numeric, operation_hash char)
AS $$
SELECT x, coalesce((SELECT sum(diff) FROM balance where contract_address = x), 0), null
UNION ALL
SELECT k, coalesce((SELECT sum(diff) FROM balance WHERE contract_address = k), 0), operation_hash FROM origination o WHERE o.source = x
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION contracts3 (x varchar)
RETURNS TABLE(k char, bal numeric, operation_hash char, delegate char)
AS $$
SELECT x, coalesce((SELECT sum(diff) FROM balance where contract_address = x), 0), null, null
UNION SELECT k, coalesce((SELECT sum(diff) FROM balance WHERE contract_address = k), 0), operation_hash, c.delegate FROM origination o, contract c WHERE o.source = x and o.k is not null and c.address = o.k
$$ LANGUAGE SQL;
