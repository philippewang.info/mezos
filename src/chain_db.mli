(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_protocol_005_PsBabyM1.Protocol.Alpha_context

(* al store_blk_full :
  *  ?chain:Block_services.chain ->
  *  ?block:Block_services.block ->
  *  #RPC_context.simple ->
  *  (module Caqti_lwt.CONNECTION) -> unit tzresult Lwt.t *)

type tx_full = {
  op_hash : Operation_hash.t ;
  id : int ;
  blk_hash : Block_hash.t ;
  level : int ;
  timestamp : Time.Protocol.t ;
  src : Contract.t ;
  src_mgr : Signature.public_key_hash option ;
  dst : Contract.t ;
  dst_mgr : Signature.public_key_hash option ;
  fee : Tez.t ;
  amount : Tez.t ;
  parameters : Script.lazy_expr option ;
}

val tx_full_encoding : tx_full Data_encoding.t

module IntMap : Map.S with type key := int

val find_txs_involving_k :
  (module Caqti_lwt.CONNECTION) -> Contract.t ->
  tx_full IntMap.t Operation_hash.Map.t Lwt.t

(* val bootstrap_chain :
 *   ?blocks_per_sql_tx:int32 ->
 *   ?from:int32 -> ?up_to:int32 ->
 *   first_alpha_level:int32 ->
 *   #full ->
 *   (module Caqti_lwt.CONNECTION) ->
 *   int32 tzresult Lwt.t *)
(** [bootstrap_chain ?blocks_per_sql_tx ?from ?up_to cctxt db]
    download and store blocks from [cctxt] in [db]. [blocks_per_sql_tx]
    governs how many blocks will be downloaded before commiting them in
    SQLite. [from] is the starting point for downloading the chain
    (default [2l]). [up_to] is a target block where to stop downloading
    blocks. *)

(* val bootstrap_delegate :
 *   #full ->
 *   (module Caqti_lwt.CONNECTION) ->
 *   Signature.Public_key_hash.t ->
 *   unit tzresult Lwt.t
 * (\** [bootstrap_delegate cctxt db delegate] downloads and stores
 *     delegate info for [delegate] from [cctxt] in [db]. *\) *)

(* val discover_initial_ks :
 *   #full ->
 *   Block_services.chain * Block_services.block ->
 *   (module Caqti_lwt.CONNECTION) ->
 *   unit tzresult Lwt.t *)
(** [discover_initial_ks cctxt blkid db] discovers all contracts
    present on the chain at [blkid] and write in [db]. *)

val history :
  ?display:bool ->
  (module Caqti_lwt.CONNECTION) ->
  Contract.t list ->
  tx_full IntMap.t Operation_hash.Map.t list tzresult Lwt.t
(** [history ?cache ?display db ks] returns the lists of transactions
    found in [db], corresponding to [k]. *)

(* val store_snapshot_levels :
 *   (module Caqti_lwt.CONNECTION) -> (int * int32) list -> unit Lwt.t *)

(* val snapshot_levels :
 *   (module Caqti_lwt.CONNECTION) -> int32 Cycle.Map.t Lwt.t *)

(** Balance updates *)

type balance_full = {
  level: int32 ;
  cycle: Cycle.t ;
  cycle_position: int32 ;
  op: (Operation_hash.t * int) option ;
  cat : Delegate.balance ;
  diff : Tez.t ;
}

val balance_full : balance_full Caqti_type.t
val select_balance_full :
  (Contract.t * int32 * int32,
   balance_full,
   Caqti_mult.zero_or_more) Caqti_request.t

(** Contracts. *)

module Contract_table : sig
  type t = {
    k: Contract.t ;
    blk_hash : Block_hash.t ;
    manager: Signature.Public_key_hash.t option ;
    delegate: Signature.Public_key_hash.t option ;
    spendable: bool ;
    delegatable: bool ;
    credit: Tez.t option ;
    preorigination: Contract.t option ;
    script: Script.t option ;
  }

  val sql_encoding : t Caqti_type.t
  val json_encoding : t Data_encoding.encoding

  val select :
    (unit, t, Caqti_mult.zero_or_more) Caqti_request.t
  val select_by_mgr :
    (public_key_hash, t, Caqti_mult.zero_or_more) Caqti_request.t
  val select_by_k :
    (Contract.t, t, Caqti_mult.zero_or_one) Caqti_request.t
end

module Contract2 : sig
  type t = {
    k: Contract.t ;
    balance: int64;
    operation_hash: Operation_hash.t option;
  }

  val sql_encoding : t Caqti_type.t
  val json_encoding : t Data_encoding.encoding

  val select :
    (Signature.Public_key_hash.t, t, Caqti_mult.zero_or_more) Caqti_request.t
end

module Contract3 : sig
  type t = {
    k: Contract.t;
    balance: int64;
    operation_hash: Operation_hash.t option;
    delegate: Signature.Public_key_hash.t option;
  }

  val sql_encoding : t Caqti_type.t
  val json_encoding : t Data_encoding.encoding

  val select :
    (Signature.Public_key_hash.t, t, Caqti_mult.zero_or_more) Caqti_request.t
end



(* (\** Delegate DB. If you need delegate info cycle per cycle. *\)
 *
 * val select_delegate :
 *   (public_key_hash,
 *    Tezos_sql.delegate_info,
 *    Caqti_mult.zero_or_more) Caqti_request.t *)



val nb_alpha_operations :
  (module Caqti_lwt.CONNECTION) -> Operation_hash.t -> int Lwt.t
