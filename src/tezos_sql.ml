(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Caqti_type.Std
open Caqti_request

let connect url =
  Caqti_lwt.connect url >>=
  Caqti_lwt.or_fail

let with_transaction conn request elts =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Conn.start () >>=
  Caqti_lwt.or_fail >>= fun () ->
  Lwt_list.iter_s begin fun elt ->
    Conn.exec request elt >>=
    Caqti_lwt.or_fail
  end elts >>= fun () ->
  Conn.commit () >>=
  Caqti_lwt.or_fail

let to_caqti_error res =
  Rresult.R.reword_error
    (fun e -> Format.asprintf "%a" pp_print_error e) res

let z =
  custom
    ~encode:(fun a -> Ok (let `Hex a = Hex.of_string (Z.to_bits a) in a))
    ~decode:(function a -> Ok (Z.of_bits (Hex.to_string (`Hex a))))
    octets

let oph =
  let open Operation_hash in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let pkh =
  let open Signature.Public_key_hash in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let pk =
  let open Signature.Public_key in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let chain_id =
  let open Chain_id in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let blk_hash =
  let open Block_hash in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let context_hash =
  let open Tezos_crypto.Context_hash in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let operation_list_hash =
  let open Operation_list_hash in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let operation_list_list_hash =
  let open Operation_list_list_hash in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let time =
  let open Time.Protocol in
  custom
    ~encode:begin fun a ->
      match Ptime.of_float_s (Int64.to_float (to_seconds a)) with
      | None -> Error "time"
      | Some v -> Ok v
    end
    ~decode:(fun a -> Ok (of_seconds (Int64.of_float (Ptime.to_float_s a))))
    ptime

let fitness =
  custom
    ~encode:(fun a -> let `Hex a = MBytes.to_hex (Fitness.to_bytes a) in Ok a)
    ~decode:(fun a -> match Fitness.of_bytes (MBytes.of_string a) with
        | Some a -> Ok a
        | None -> Error "fitness")
    octets

let shell_header =
  let open Block_header in
  custom
    ~encode:begin fun {
      level ; proto_level ; predecessor ; timestamp ;
      validation_passes ; operations_hash ; fitness ; context } ->
      Ok ((level, proto_level, predecessor, timestamp),
          (validation_passes, operations_hash, fitness, context))
    end
    ~decode:begin fun
      ((level, proto_level, predecessor, timestamp),
       (validation_passes, operations_hash, fitness, context)) ->
      Ok { level ; proto_level ; predecessor ; timestamp ;
           validation_passes ; operations_hash ; fitness ; context }
    end
    (tup2
       (tup4 int32 int blk_hash time)
       (tup4 int operation_list_list_hash fitness context_hash))

let json =
  custom
    ~encode:(fun json -> Ok (Data_encoding.Json.to_string json))
    ~decode:Data_encoding.Json.from_string
    string

open Tezos_protocol_005_PsBabyM1
open Protocol
open Alpha_context
(* open Alpha_environment *)

let k =
  custom
    ~encode:(fun a -> Ok (Contract.to_b58check a))
    ~decode:(fun a -> to_caqti_error (Environment.wrap_error (Contract.of_b58check a)))
    string

(* let kh =
 *   custom
 *     ~encode:(fun a -> Ok (Contract_hash.to_b58check a))
 *     ~decode:begin fun a -> match Contract_hash.of_b58check_opt a with
 *       | None -> Error "kh"
 *       | Some kh -> Ok kh
 *     end
 *     string *)

let tez =
  custom
    ~encode:(fun a -> Ok (Tez.to_mutez a))
    ~decode:begin fun a ->
      match Tez.of_mutez a with
      | None -> Error "tez"
      | Some t -> Ok t
    end
    int64

let lazy_expr_of_json json =
  Data_encoding.Json.destruct Script.lazy_expr_encoding json

let json_of_lazy_expr expr =
  Data_encoding.Json.construct Script.lazy_expr_encoding expr

let lazy_expr =
  custom
    ~encode:(fun a -> Ok (json_of_lazy_expr a))
    ~decode:(fun a -> Ok (lazy_expr_of_json a))
    json

let script_of_json json =
  Data_encoding.Json.destruct Script.encoding json

let json_of_script expr =
  Data_encoding.Json.construct Script.encoding expr

let script =
  custom
    ~encode:(fun a -> Ok (json_of_script a))
    ~decode:(fun a -> Ok (script_of_json a))
    json

let balance =
  custom
    ~encode:begin function
      | Delegate.Contract _ -> Ok 0
      | Rewards _ -> Ok 1
      | Fees _ -> Ok 2
      | Deposits _ -> Ok 3
    end
    ~decode:(fun _ -> Error "decode not supported")
    int

let balance_update =
  custom
    ~encode:begin function
      | Delegate.Credited t -> Ok (Tez.to_mutez t)
      | Debited t -> Ok (Int64.neg (Tez.to_mutez t))
    end
    ~decode:begin fun t ->
      let positive = t > 0L in
      match Tez.of_mutez (Int64.abs t) with
      | None -> Error "of_mutez"
      | Some t ->
        if positive then Ok (Delegate.Credited t)
        else Ok (Debited t)
    end
    int64

let cycle =
  custom
    ~encode:(fun a -> Ok (Cycle.to_int32 a))
    ~decode:(fun a -> Ok (Obj.magic a))
    int32

let voting_period =
  custom
    ~encode:(fun a -> Ok (Voting_period.to_int32 a))
    ~decode:(fun a -> Ok (Obj.magic a))
    int32

let voting_period_kind =
  custom
    ~encode:(fun a -> Ok (Obj.magic a))
    ~decode:(fun a -> Ok (Obj.magic a))
    int



type delegate_info = {
  cycle: Cycle.t ;
  level: int32 ;
  pkh: public_key_hash ;
  info: Delegate_services.info ;
}

let delegate_info =
  custom
    ~encode:begin
      fun { cycle; level; pkh; info = { Delegate_services.balance; frozen_balance;
                                        frozen_balance_by_cycle = _; staking_balance;
                                        delegated_contracts = _;
                                        delegated_balance;
                                        deactivated; grace_period } } ->
        Ok ((cycle, level, pkh),
            (balance, frozen_balance, staking_balance, delegated_balance),
            (deactivated, grace_period))
    end
    ~decode:begin
      fun ((cycle, level, pkh),
            (balance, frozen_balance, staking_balance, delegated_balance),
           (deactivated, grace_period)) ->
        let info = { Delegate_services.balance ; frozen_balance ;
                     frozen_balance_by_cycle = Cycle.Map.empty ;
                     staking_balance ; delegated_contracts = [] ;
                     delegated_balance ; deactivated ; grace_period } in
        Ok { cycle; level; pkh; info }
    end
    (tup3
       (tup3 cycle int32 pkh)
       (tup4 tez tez tez tez)
       (tup2 bool cycle))
