all:
	@echo To get tezos-indexer:
	@echo make tezos-indexer
	@echo To compile Mezos for BabyM1, run:
	@echo ${MAKE} 5
	@echo and for Carthagenet, run:
	@echo ${MAKE} 6



tezos-indexer: source-deps

source-deps:
	git clone https://gitlab.com/nomadic-labs/tezos-indexer.git -b multinetwork-dev
	cd tezos-indexer && make tezos && make protos
	sed -e 's/(source_tree %{workspace_root})//' tezos-indexer/tezos/src/lib_version/dune > tezos-indexer/tezos/src/lib_version/dune.tmp && mv tezos-indexer/tezos/src/lib_version/dune.tmp tezos-indexer/tezos/src/lib_version/dune
	cd tezos-indexer/tezos && git fetch https://gitlab.com/philippewang.info/tezos.git 4e3944895b70ff6d8ba430c23cc9f6f757f58c61 && git cherry-pick 4e3944895b70ff6d8ba430c23cc9f6f757f58c61
	@echo Now to compile Mezos for BabyM1, run:
	@echo ${MAKE} 5
	@echo and for Carthagenet, run:
	@echo ${MAKE} 6



6:
	find src -iname '*.ml' -or -iname '*.mli' | while read l ; do sed -e 's/005-PsBabyM1/006-PsCARTHA/g' -e 's/005_PsBabyM1/006_PsCARTHA/g' $$l > $$l.tmp ; mv $$l.tmp $$l ; done
	TEZOS_PROTO=6 dune build @install
	${MAKE} mezos.exe

5:
	find src -iname '*.ml' -or -iname '*.mli' | while read l ; do sed -e 's/006-PsCARTHA/005-PsBabyM1/g' -e 's/006_PsCARTHA/005_PsBabyM1/g' $$l > $$l.tmp ; mv $$l.tmp $$l ; done
	TEZOS_PROTO=5 dune build @install
	${MAKE} mezos.exe

mezos.exe:
	cp ./_build/default/src/mezos.exe $@

4:
	TEZOS_PROTO=4 dune build @install

3:
	TEZOS_PROTO=3 dune build @install

2:
	TEZOS_PROTO=2 dune build @install

1:
	TEZOS_PROTO=1 dune build @install

clean:
	dune clean

distclean:
	rm -fr tezos-indexer/
